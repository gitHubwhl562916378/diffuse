#version 330
uniform mat4 uPMatrix,camMatrix,uMMatrix;
uniform vec3 uLightLocation;
layout (location = 0) in vec3 aPosition;
layout (location = 1) in vec3 aNormal;
smooth out vec3 vPosition;
smooth out vec4 vdiffuse;

void pointLight(in vec3 normal,inout vec4 diffuse,in vec4 lightDiffuse){
    vec3 normalTarget = aPosition + normal;
    vec3 newNormal = normalize((uMMatrix * vec4(normalTarget, 1)).xyz -(uMMatrix * vec4(aPosition,1)).xyz);

    vec3 vp = normalize(uLightLocation - (uMMatrix * vec4(aPosition,1)).xyz);
    float nDotViewPosition = max(0.0,dot(newNormal,vp));
    diffuse = lightDiffuse * nDotViewPosition;
}
void main(void)
{
    gl_Position = uPMatrix * camMatrix *uMMatrix * vec4(aPosition,1);
    vec4 diffuseTemp = vec4(0.0,0.0,0.0,0.0);
    pointLight(aNormal,diffuseTemp,vec4(0.8,0.8,0.8,1.0));
    vdiffuse = diffuseTemp;
    vPosition = aPosition;
}
