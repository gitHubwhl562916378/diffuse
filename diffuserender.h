#ifndef DIFFUSERENDER_H
#define DIFFUSERENDER_H

#include <QOpenGLExtraFunctions>
#include <QOpenGLShaderProgram>
#include <QOpenGLBuffer>
#define PI 3.14159265f
class DiffuseRender
{
public:
    DiffuseRender() = default;
    void initialize(float r);
    void render(QOpenGLExtraFunctions *f,QMatrix4x4 &projM,QMatrix4x4 & camera,QMatrix4x4 &model,QVector3D &lightLocation);

private:
    QOpenGLShaderProgram m_program;
    QOpenGLBuffer m_vbo;
    QVector<GLfloat> m_points;
    float m_r;
};

#endif // DIFFUSERENDER_H
